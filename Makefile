SRC_DIR = src/
LIB_DIR = lib/
STA_EXE = bin/calculadora_e
DYN_EXE = bin/calculadora_d

all:
		mkdir -p bin && make -C $(LIB_DIR) && make -C $(SRC_DIR)

run:
		$(STA_EXE)
		$(DYN_EXE)

clean:
		rm -r bin
		rm lib/*.o
		rm lib/*.a
		rm lib/*.so
		clear

